# Test automatique exerice C/CPP


Nowledgeable met à disposition un ensemble de fonction qui permet de corriger automatique 
les exercices de C et CPP 

## la structure d'un main de test

Les exercices sont tests dans le main. 
Il doit contenir

1. Une variable contenant un pointeur vers une struture Output
2. Des tests (optionnel)
3. l'appel à la fonction ```c print_output_json(Output ouput)```

Voici un exemple ci-dessous : 

### Exemple 

```c 
int main(){
                
    Output* output = new_Output();                         
     
   /** Question 1 **/

    int result = fonction_de_letudiant()
    int expected_result = 1;
    
    compare_ints(expected, result, output, "message décrivant le test");

    print_output_json(*output);
}

```

## Liste des fonctions mis à disposition

### Comparer des valeurs de variables

```c
//comparaison type tableau
void compare_ints(int expected, int submitted, Output* out, const char feedback[]
void compare_floats(float expected, float submitted, Output* out, const char feedback[])

//comparaison tableaux
void compare_float_array(float* expected, float* submitted, int n_values, Output* out, const char feedback[]);  

void compare_strings(const char* expected, const char* submitted, Output* out, const char test_description[]);

```


### Créer des valeur aléatoires

```c
int generate_random_int(int min, int max)
float generate_random_float(float min, float max)
float* generate_random_float_array(float min, float max, int n_values)
int* generate_random_int_array(float min, float max, int n_values)
char* generate_random_string_from_words(int n_word, char words[][100])
```


