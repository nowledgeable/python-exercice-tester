# Correction automatique exercice python

Afin qu'un exercice dispose d'une correction automatique, 
il faut que le script affiche un json au format suivant 

```json
{
   'isRight': true|false|0|1, //indique si la réponse est globalement juste ounon
   'print': "string",
   'tests': [
        {
            "name": "nom du test",
            'isRight': true|false|0|1,
            'expected': "value" //optionnel,
            'submitted': "value" //optionnel,
            "message": "message si le test est faux"
        },
        ...
    ]
}
```
Le champs tests contient une liste d'assertions à propos
du code de l'étudiant. 

Pour générer facilement ce json,  la class `AnswerTester` est mise à disposition.
Elle permet de définir des assertions testant le code de l'étudiant et 
d'afficher le json correspondant. 

## Utilisation

### instanciation et récupération du json

```python
tester = AnswerTester()
# assertions à mettre ici
tester.print_test_output()
```

### liste des assertions 

-  `compare_variables` : permet de vérifier qu'une variable est égale à une autre
-  `compare_functions` : permet de vérifier que le retour d'une fonction correspond à une autre sur un jeu de données
-  `add_assert`        : permet de vérifier qu'une assertion est vraie
-  `assert_A_or_B`     : permet de vérifier qu'une assertion A ou B est  vraie
-  `was_function_called_with` : permet de vérifier qu'une fonction donnée a été appelée avec des arguments spécifiées


## Exemples

### compare_variables 

```python

a = 3 

tester = AnswerTester()

tester.compare_variables('a = 2', a, 2)

tester.print_test_output()
```

### utilisation de add_assert

Imaginons que nous voulions verifier qu'une variable `v` a une valeur inferieur
a 10 et qu'elle est de type float, nous pourrions faire comme suit 

```python
tester.add_assert("check v is below 10", v < 10, "v should be strictly lower than 10 !")
tester.add_assert("check type of v", isinstance(v, (float,)), "v should be a float number")
```

En gros le second argument doit être un boolean, obtenu de n'importe quelle manière
qui permet de vérifier si une proposition est vrai. 

### Exercice où l'étudiant code une fonction avec compare_functions 

prennons l'énoncé suivant : 

"Coder la fonction multiply_by_two(x) qui calcule 2 * x pour chaque x"

Le code de test pourra être comme ci dessous. 
On utilise la méthode `compare_functions`.
Pour cela il faut coder la fonction solution.
`compare_functions` va comparer la sortie de la fonction de l'étudiant
et de la fonction solution sur plusieurs jeux d'entrée

`test_inputs` représenté les entrée de test. 
C'est une liste de liste.
Chaque sous tableau représente un test et contient les arguments de la fonction

```python

def solution(x): return 2 * x

tester = AnswerTester()

test_inputs = [
    [3], # la fonction sera testée avec l'argument x=3
    [-2], # la fonction sera testée avec l'argument x=-2
    [random.randint(-3, 3)] #la fonction sera testée avec un x aléatoire entre -3 et 3
]

tester.compare_functions("nom test", multiply_by_two, solution, test_inputs, "message")

result_status = tester.get_test_output()

result = tester.get_test_output() #A la fin on demande de générer le json
```

### utilisation de was_function_called_with

la class AnswerTester est capable de récupérer l'ensemble des fonctions
appelée par l'étudiant lors de l'éxecution de sa réponse. 

Un enseignant peut donc vérifier si l'étudiant a bien appelé une fonction 
donnée avec les **valeurs** d'argument adéquat. 

Imaginons qu'on veuille vérifier que la fonction poulet(a, b) a été
appelée avec a = 2 et b = 4 à un moment dans le script. 
il suffira de faire comme suit 
```python
tester.was_function_called_with('poulet a été apelée avec 2 et 4', 'poulet', {'a': 2, "b": 4})
```

Si l'étudiant n'a pas appelé poulet ça reverra faux. 
Cela renverra également faux si la fonction poulet a été appelé avec 
des valeurs différents. 

Cela marche aussi pour les fonctions de librairies

Pour kwarg il faut préciser l'argument dans le dictionnaire sans * :

```python
tester.was_function_called_with('poulet a été apelée avec 2 et 4', 'poulet', {'kwargs': etc })
```
